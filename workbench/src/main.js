import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { sync } from 'vuex-router-sync'
import vuetify from './plugins/vuetify';
import 'vuetify/dist/vuetify.min.css';
import VueYoutube from 'vue-youtube'
import Eagle from 'eagle.js'
//import 'animate.css' // import animate.css for slide transition 
import VModal from 'vue-js-modal'
import VueHtmlToPaper from "vue-html-to-paper"


Vue.use(VModal, { dynamic: true, dynamicDefaults: { clickToClose: true } })
Vue.use(Eagle)
Vue.use(VueYoutube)
const options = { // this is for page printing
  name: "_blank",
  specs: ["fullscreen=yes", "titlebar=yes", "scrollbars=yes"],
  styles: [
    "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css",
    "https://unpkg.com/kidlat-css/css/kidlat.css"
  ]
};

Vue.use(VueHtmlToPaper, options)

Vue.config.productionTip = false
sync(store, router)

new Vue({
  store,
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
