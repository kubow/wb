import '@mdi/font/css/materialdesignicons.css'
import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import FlagIcon from 'vue-flag-icon'

Vue.use(Vuetify);
Vue.use(FlagIcon);

const opts = {
  icons: {
    iconfont: 'mdi',
    theme: {
      primary: '#844d36',
      success: '#99738e',
      info: '#fce181',
      error: '#e27d60'

    }
  },
}

export default new Vuetify(opts);
