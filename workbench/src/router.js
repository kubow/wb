import Vue from 'vue'
import Router from 'vue-router'
//here comes list of components
import Home from '@/components/Home'
import Login from '@/components/Login'
import News from '@/components/News'
import Offer from '@/components/Offer'
import Videos from '@/components/Videos'
import Sent from '@/components/Sent'
import WB from '@/components/WB'
import Config from '@/components/Config'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/news',
      name: 'News',
      component: News
    },
    {
      path: '/offer',
      name: 'Offer',
      component: Offer
    },
    {
      path: '/video',
      name: 'Videos',
      component: Videos
    },
    {
      path: '/sent',
      name: 'Sent',
      component: Sent
    },
    {
      path: '/config',
      name: 'Config',
      component: Config
    },
    {
      path: '/wb/:id',
      name: 'WB',
      component: WB
    }
  ]
})
