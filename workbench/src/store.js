﻿import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate'
import axios from 'axios'

Vue.use(Vuex);

const state = {
    activeLanguage: {text: "čeština", value: "cz"},
    currentSelect: "0000",
    wbSearch: "",
    goldenRatio: ( 1 + Math.sqrt(5)) / 2, // For positioning
    presentation: [
        {src: "background_bench_cut.jpg", alt: "cutted"},
        {src: "background_bench.jpg", alt: "original"},
        {src: "wood-v.jpg", alt: "drevo"}
    ],
    tableData: {}, //data items across site
    tableData2: {}, //xxx
    category: [], //item category list
    news: [],
    addons: [],
    translation: {}, // language logic implemented here
    // authorization - not implemented yet
    token: localStorage.getItem('user-token') || '',
    status: '',
    user: null,
    isUserLoggedIn: false,
    profile: {}
}

const getters = {
    activeLanguage(state) {
        return state.activeLanguage
    },
    activeSelect(state) {
        return state.currentSelect
    },
    menuItems(state) { 
        var m_items = filterItems(state.translation,"menu")
        var menuIt = []
        Object.entries(m_items).forEach(([key, value]) => { 
            menuIt.push({
                text: value,
                id: key[key.length -1], 
                path: buildMenuPath(key[key.length -1]),
                icon: buildMenuIcon(key[key.length -1])
            });
        }); 
        return menuIt
        //return filterItems(state.translation,"menu")
    },
    presentation(state) {
        return state.presentation
    },
    selected(state) {
        return state.currentSelect
    },
    searched(state) {
        return state.wbSearch
    },
    t_addons(state) {
        return state.addons
    },
    t_basic(state) {
        return state.tableData.basic
    },
    t_cats(state) {
        return state.category
    },
    t_catalog(state) {
        return state.tableData.catalog
    },
    t_news(state) {
        return state.news
    },
    t_trans(state) {
        for(let t_obj of state.tableData.translation){
            if (state.activeLanguage.value == "cz") {
                state.translation[t_obj.feature_name] = t_obj.s_cz
            } else if (state.activeLanguage.value == "de") {
                state.translation[t_obj.feature_name] = t_obj.s_de
            } else if (state.activeLanguage.value == "gb") {
                state.translation[t_obj.feature_name] = t_obj.s_gb
            } else {
                state.translation = {}
            }
          }
        return state.translation
    },
    t_variants(state) { // no language mutation
        return state.tableData.catalog.filter(item => item.category == 0)
    },
    // authentication stuff
    authStatus: state => state.status,
    getProfile: state => state.profile,
    isAuthenticated: state => !!state.token,
    isProfileLoaded: state => !!state.profile.name,
}

const mutations = {
    SET_DATA(state) {
        state.tableData = require('./assets/data.json')
        axios.post('w_server.php', {
            request: 1
          })
          .then(function (response) {
            state.tableData2 = response.data;
          })
          .catch(function (error) {
            console.log(error);
          });     
    },
    SET_LANG(state, language) {
        if (language) {
            state.activeLanguage = language
        } else {
            // eslint-disable-next-line no-console
            console.log(sessionStorage.getItem("vuex"))
            state.activeLanguage.value = activeL()
            state.activeLanguage.text = languageName(state.activeLanguage.value)
        }
        
        state.translation = {}
        for (let t_obj of state.tableData.translation){
            if (state.activeLanguage.value == "cz") {
                state.translation[t_obj.feature_name] = t_obj.s_cz
            } else if (state.activeLanguage.value == "de") {
                state.translation[t_obj.feature_name] = t_obj.s_de
            } else if (state.activeLanguage.value == "gb") {
                state.translation[t_obj.feature_name] = t_obj.s_gb
            }
        }
        var category = []
        for (let c_obj of state.tableData.addons_cat){
            if (state.activeLanguage.value == "cz") {
                category.push({"id":c_obj.id, "string": c_obj.s_cz, "info": ""})
            } else if (state.activeLanguage.value == "de") {
                category.push({"id":c_obj.id, "string": c_obj.s_de, "info": ""})
            } else if (state.activeLanguage.value == "gb") {
                category.push({"id":c_obj.id, "string": c_obj.s_gb, "info": ""})
            }
        }
        state.category = category
        var news = []
        for (let n of state.tableData.news) {
            if (state.activeLanguage.value == "cz") {
                news.push({"id":n.id, "created": n.created, "valid": n.valid, "string": n.s_cz, "info": n.info_cz})
            } else if (state.activeLanguage.value == "de") {
                news.push({"id":n.id, "created": n.created, "valid": n.valid, "string": n.s_de, "info": n.info_de})
            } else if (state.activeLanguage.value == "gb") {
                news.push({"id":n.id, "created": n.created, "valid": n.valid, "string": n.s_gb, "info": n.info_gb})
            }
        }
        state.news = news
        var addons = []
        for (let n of state.tableData.catalog.filter(item => item.category != 0)) {
            if (state.activeLanguage.value == "cz") {
                addons.push({"id":n.id, "code": n.code, "price": n.price_cz, "string": n.s_cz, "info": n.desc_cz, "addon": n.addon_cat})
            } else if (state.activeLanguage.value == "de") {
                addons.push({"id":n.id, "code": n.code, "price": n.price_cz, "string": n.s_de, "info": n.desc_de, "addon": n.addon_cat})
            } else if (state.activeLanguage.value == "gb") {
                addons.push({"id":n.id, "code": n.code, "price": n.price_cz, "string": n.s_gb, "info": n.desc_gb, "addon": n.addon_cat})
            }
        }
        state.addons = addons

    },
    SET_SELECT(state, value) {
        state.currentSelect = value
    },
    SET_TOKEN (state, token) {
        state.token = token
        state.isUserLoggedIn = !!(token)
    },
    SET_USER (state, user) {
        state.user = user
    },
    TRANSLATE (state, table) {
        for(let o of table){
            if (state.activeLanguage == "cz") {
                table.string = o.s_cz
                table.information = o.info_cz
            } else if (state.activeLanguage == "de") {
                table.string = o.s_de
                table.information = o.info_de
            } else if (state.activeLanguage == "gb") {
                table.string = o.s_gb
                table.information = o.info_gb
            }
        }
    }
}

const actions = {
    prepareContent({ commit }) { // action to prepare table data
        commit("SET_DATA") 
        commit("SET_LANG")
    },
    preparePersisted({ commit }) { // action to prepare table data in persistent state
        let lang = JSON.parse(sessionStorage.getItem("vuex"))
        commit("SET_DATA") 
        commit("SET_LANG", lang.activeLanguage)
    },
    changeLang({ commit }, newLang) {
        commit('SET_LANG', newLang)    
    },
    setToken ({commit}, token) {
        commit('SET_TOKEN', token)
    },
    setUser ({commit}, user) {
        commit('SET_USER', user)
    },
    tableSelect({ commit}, code) {
        commit('SET_SELECT', code)
    }
}

function activeL(lang) { // browser default language
    var nl
    if (lang) {
        if (checkInList(["cz","cs"],lang)) {
            nl = "cz";
        } else if (checkInList(["en","us","gb"],lang)) {
            nl = "gb"
        } else if (checkInList(["de","ge"],lang)) {
            nl = "de";
        }
    } else {
        //var lang //= "en"; //basic fallback 
        if (checkInList(["cz","cs"],Intl.NumberFormat().resolvedOptions().locale.slice(0,2))) {
            nl = "cz";
        } else if (checkInList(["en","us"],Intl.NumberFormat().resolvedOptions().locale.slice(0,2))) {
            nl = "gb"
        } else if (checkInList(["de"],Intl.NumberFormat().resolvedOptions().locale.slice(0,2))) {
            nl = Intl.NumberFormat().resolvedOptions().locale.slice(0,2);
        }
    }
    // eslint-disable-next-line no-console
    console.log("returning", nl)
    return nl
}
function languageName(lang) {
    if (checkInList(["cz","cs"],lang)) {
        return "čeština"
    } else if (checkInList(["en","us", "gb"],lang)) {
        return "english"
    } else if (checkInList(["de", "ge"],lang)) {
        return "deutsch"
    } else {
        return "lang "+lang+" not known"
    }
}
function checkInList(arr, val) { // helper for comparing arrays
    return arr.some(arrVal => val === arrVal);
}
function filterItems(object, string) { // helper for filtering strings
    var items = object;
        var result = {}
        Object.keys(items).forEach(key => {
            const item = items[key]
            if (key.search(string) !== -1 ) {
              result[key] = item
            }
          })  
        return result
}
function buildMenuPath(idNum) { //build menu item paths
    switch(idNum) {
        case "5":
            return "/sent"
        case "4":
            return "/video"
        case "3":
            return "/news"
        case "2":
            return "/offer"
        default:
          return "/"
      }
}
function buildMenuIcon(idNum) { //build menu item icons
    switch(idNum) {
        case "5":
            return "mdi-contact-mail"
        case "4":
            return "mdi-video"
        case "3":
            return "mdi-new-box"
        case "2":
            return "mdi-shopping"
        default:
          return "mdi-package-variant"
      }
}

const store = new Vuex.Store({
//export default new Vuex.Store({
//export const store = new Vuex.Store({
    strict: true,
    plugins: [
        createPersistedState({
            storage: window.sessionStorage,
            reducer(val) {
                return {
                    //only store active language setting
                    activeLanguage: val.activeLanguage,
                    wbSearch: val.wbSearch
                }
            }
        })
      ],
    state,
    getters,
    mutations,
    actions
});

export default store;
